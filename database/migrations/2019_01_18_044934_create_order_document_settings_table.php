<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDocumentSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_document_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document_code')->unique();
            $table->string('field_name');
            $table->string('document_storage_path');
            $table->string('document_file_type');
            $table->enum('is_required',['0','1'])->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_document_settings');
    }
}
