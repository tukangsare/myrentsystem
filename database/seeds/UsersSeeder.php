<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role();
        $role_admin->name = 'admin';
        $role_admin->desc = 'Khusus Admin';
        $role_admin->save();


        $admin_person = new User([
            'name' => 'admin',
            'username'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>bcrypt('admin1'),
            'is_active'=>true
        ]);
        $admin_person->save();
        $role_admin->users()->save($admin_person);
        $admin_person->roles()->associate($role_admin)->save();


        $role_user = new Role();
        $role_user->name = 'user';
        $role_user->desc = 'Buat User';
        $role_user->save();

        $user_person = new User([
            'name' => 'user',
            'username'=>'user',
            'email'=>'user@gmail.com',
            'password'=>bcrypt('user01'),
            'is_active'=>true
        ]);

        $role_user->users()->save($user_person);
        $user_person->roles()->associate($role_user)->save();

        $role_manager = new Role();
        $role_manager->name = 'manager';
        $role_manager->desc = 'Buat Manager';
        $role_manager->save();

        $manager_person = new User([
            'name' => 'manager',
            'username'=>'manager',
            'email'=>'manager@gmail.com',
            'password'=>bcrypt('manager1'),
            'is_active'=>true
        ]);

        $role_manager->users()->save($manager_person);
        $manager_person->roles()->associate($role_manager)->save();
    }
}
