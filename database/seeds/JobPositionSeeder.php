<?php

use App\JobPosition;
use Illuminate\Database\Seeder;

class JobPositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job_manager = JobPosition::create([
            'job_code'  => 'MS-010',
            'job_name'  => 'Manager Marketing',
            'job_desc'  => 'Masa gatau tugas nya manager?'
        ]);
        $job_marketing = JobPosition::create([
            'job_code'  => 'MS-011',
            'job_name'  => 'Marketing',
            'job_desc'  => 'Masa gatau tugas nya marketing?'
        ]);
//        $job = JobPosition::create([
//            'job_code'  => 'MS-012',
//            'job_name'  => 'Admin Marketing',
//            'job_desc'  => 'Masa gatau tugas nya admin?'
//        ]);
    }
}
