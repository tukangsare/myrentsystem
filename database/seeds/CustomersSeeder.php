<?php

use Illuminate\Database\Seeder;
use App\Customer;
class CustomersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer1 = Customer::create([
            'code'      => 'C-00001',
            'name'      => 'Anto Wijaya',
            'address'   => 'Jl. Cipto no. 69, Kota Cirebon',
            'phone'     => '081324453777',
            'email'     => 'antowidjaja@gmail.com'
        ]);
        $customer2 = Customer::create([
            'code'      => 'C-00002',
            'name'      => 'PT Melia Sira',
            'address'   => 'Jl. Pegangsan no. 69, Kota Cirebon',
            'phone'     => '023132456789',
            'email'     => 'meliasira@namspace.com'
        ]);
    }
}
