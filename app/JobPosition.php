<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobPosition extends Model
{
    protected $fillable = [
        'job_code', 'job_name', 'job_desc'
    ];
    public function employee()
    {
        return $this->hasMany('App\Employee');
    }
}
