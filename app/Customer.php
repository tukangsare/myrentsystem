<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
        'code','name','email','phone','address','ktp','description'
    ];
    public function getNameAttribute ($name)
    {
        return strtoupper($name);
	}
	public function orders()
	{
		return $this->hasMany('App\Order');
	}
}
