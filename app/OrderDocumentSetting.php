<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDocumentSetting extends Model
{
    const AVAILABLE_FILE_EXTS = [
            '.jpg'  => 'Gambar (.jpg)',
            '.pdf'  => 'Dokumen (.pdf)'
    ];

    protected $fillable = [
        'document_code','field_name','document_storage_path','document_file_type','is_required'
    ];
    public function orderdocuments()
    {
        return $this->hasMany('App\OrderDocument');
    }
    public function orders()
    {
        return $this->belongsToMany('App\Order','order_documents')->withPivot(['id','is_accepted']);
    }

    public function getIsRequiredAttribute($is_required)
    {
        return ($is_required === '1')?'Ya':'Tidak';
    }
    static function getExtFiles($extindex)
    {
        return Self::AVAILABLE_FILE_EXTS[$extindex];
    }
}
