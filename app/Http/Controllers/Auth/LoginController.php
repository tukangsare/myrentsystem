<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $adminRedirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function authenticated(Request $request, $user)
    {
            // untuk mengecek apakah akun aktif, jika belum aktif maka cek email
            if (!$user->is_active == true)
            {
                Auth::logout();
                return back()->with('warning','Maaf akun kamu belum aktif, silahkan check email untuk mengkonfirmasi akun');
            }
            // jika status role=1 (admin) jika 2 maka dia user biasa
            if ($user->role_id == '2')
            {
                return redirect()->intended($this->redirectTo);
            }
                return redirect()->intended($this->adminRedirectTo);
    }
}
