<?php

namespace App\Http\Controllers\Admin;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductDetail;
class ProductDetailController extends Controller
{
    public function __construct()
    {
        $this->storagePath = 'images/product/detail/';
        $this->image_prefix = 'product_detail_image';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        if ($request->cari) {
            $cari = $request->cari;
			$p = Product::with('product_details', function ($query) use($cari) {
                    $query->where('product_detail_code','LIKE',"%$cari%")
							->orWhere('engine_code','LIKE',"%$cari%")
							->orWhere('color','LIKE',"%$cari%")
							->orWhere('nopol','LIKE',"%$cari%")
							->orWhere('desc','LIKE',"%$cari%");
					})
				->where('id',$id)
                ->orderBy('code','asc')
                ->paginate(10);
        } else {
            $p = Product::where('id',$id)->with('product_details')->orderBy('code','asc')->paginate(10);
		}
        return view('admin.product_detail.index',[
			'products'     	=> $p,
			'product_id'	=> $id,
            'total_data'    => ProductDetail::where('product_id',$id)->count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product_detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$prod_id)
    {
		$validate = Validator::make($request->all(),
		[
            'engine_code' 	=> 'required',
            'nopol' 		=> 'required',
			'color' 		=> 'required',
			'code'			=> 'required',
		]);
        if (!$validate->fails()) {
			$product 	= Product::findOrFail($prod_id);
            $p_detail 	= new ProductDetail([
                'product_id'			=> $prod_id,
                'product_detail_code'	=> $request->code,
                'engine_code'			=> $request->engine_code,
                'color' 				=> $request->color,
                'nopol' 				=> $request->nopol,
                'desc'	 				=> $request->desc,
			]);
            $saveImage= $this->saveImage($request,'product_image');
            if ($saveImage)
            {
                $p_detail->product_images = $saveImage;
            }
			$p_detail->products()->associate($product);
            if ($p_detail->save()) {
                return redirect('/admin/p/'.$prod_id.'/detail')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($prod_id,$id)
    {
		$prod_detail = ProductDetail::findOrFail($id);
		return view('admin.product_detail.edit', ['detail' => $prod_detail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $prod_id, $id)
    {
		$validate = Validator::make($request->all(),
		[
            'engine_code' 	=> 'required',
            'nopol' 		=> 'required',
			'color' 		=> 'required',
		]);
        if (!$validate->fails()) {
			$product 	= Product::findOrFail($prod_id);
			$data = [
                'engine_code'			=> $request->engine_code,
                'color' 				=> $request->color,
                'nopol' 				=> $request->nopol,
                'desc'	 				=> $request->desc,
            ];
			$saveImage = $this->saveImage($request,'product_image');
			if ($saveImage)
            {
                $data['product_images'] = $saveImage;
            }
			$p_detail 	= ProductDetail::where('id',$id)
				->update();
            if ($p_detail) {
                return redirect('/admin/p/'.$prod_id.'/detail')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($prod_id,$id)
    {
		$c = ProductDetail::destroy($id);
		return response()->json(['status' => (bool) $c]);
    }
}
