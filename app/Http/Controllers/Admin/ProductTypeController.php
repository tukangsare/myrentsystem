<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Helpers\StringGenerator;
use App\ProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Validator;
class ProductTypeController extends Controller
{
    function __construct()
    {
        $this->storagePath = 'images/product-type/thumbnail/';
        $this->image_prefix = 'product_type_thumbnail';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $types = ProductType::where('name','LIKE',"%$cari%")
                ->orderBy('name','asc')
                ->paginate(10);
        } else {
            $types = ProductType::orderBy('name','asc')->paginate(10);
        }
            return view('admin.product_type.index',[
                'types'     => $types,
                'total_data'    => ProductType::count(),
                'request'       => $request
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required',
        ]);
        $saveImage = $this->saveImage($request,'thumbnail');

        if (!$validate->fails() && $saveImage) {
            $save = ProductType::create([
                    'name'      => $request->name,
                    'thumbnails' => $saveImage
                ]);
            if ($save)
            {
                return redirect('/admin/product-type')->with([
                    'status'    => 'success',
                    'msg'       => "<strong>Selamat!</strong> Data berhasil ditambah"
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $type = ProductType::findOrFail($id);
        if (!$type) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak administrator'
                ]);
        }
        return view('admin.product_type.edit',['type' => $type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required',
        ]);
        if (!$validate->fails())
        {
            $data = [
                'name' => $request->name,
            ];
            $saveImage = $this->saveImage($request,'thumbnail');
            if($saveImage)
            {
                $data['thumbnails'] = $saveImage;
//                return $data;
            }
            $save = ProductType::where('id',$id)
                ->update($data);
            if ($save) {
                return redirect('/admin/product-type')->with([
                    'status'    => 'success',
                    'msg'       => "<strong>Selamat!</strong> Data berhasil diupdate"
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = ProductType::destroy($id);
        return response()->json(['status' => (bool) $destroy]);

    }
}
