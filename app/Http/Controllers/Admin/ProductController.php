<?php
namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProductType;
use Validator;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->storagePath = 'images/product/images/';
        $this->image_prefix = 'product_thumbnail';
    }

    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $types = Product::where('code','LIKE',"%$cari%")
                ->orWhere('name','LIKE',"%$cari%")
                ->orWhere('price','LIKE',"%$cari%")
                ->orWhereHas('product_types', function ($query) use($cari) {
                    $query->where('name','LIKE',"%$cari%");
                })
                ->orderBy('code','asc')
                ->paginate(10);
        } else {
            $types = Product::with('product_types')->orderBy('code','asc')->paginate(10);
        }
        return view('admin.product.index',[
            'types'     => $types,
            'total_data'    => Product::count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$product_types = ProductType::all();
		return view('admin.product.create',['product_types' => $product_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validate = Validator::make($request->all(),
		[
            'name' 			=> 'required',
            'code' 			=> 'required',
			'product_type' 	=> 'required',
			'price'			=> 'required'
        ]);
        if (!$validate->fails())
        {
			$product_types = ProductType::findOrFail($request->product_type);
            $product = new Product([
                'code' 				=> $request->code,
                'name' 				=> $request->name,
                'product_type_id' 	=> $request->product_type,
                'price' 			=> $request->price
			]);

            $saveImage = $this->saveImage($request,'image');
            if ($saveImage) {
                $product->images = $saveImage;
            }

            $product->product_types()->associate($product_types);
            if ($product->save()) {
                return redirect('/admin/product')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$product 		= Product::findOrFail($id);
		$product_types 	= ProductType::all();
        if (!$product) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak administrator'
                ]);
        }
		return view('admin.product.edit', [
				'product' => $product,
				'product_types' => $product_types,
			]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validate = Validator::make($request->all(),
		[
            'name' 			=> 'required',
			'product_type' 	=> 'required',
			'price'			=> 'required'
        ]);
        if (!$validate->fails()) {
			$product_types = ProductType::findOrFail($request->product_type);
			$data = [
                'name' 				=> $request->name,
                'product_type_id' 	=> $request->product_type,
                'price' 			=> $request->price,
            ];
			$saveImage = $this->saveImage($request,'image');
			if ($saveImage)
            {
                $data['images'] = $saveImage;
            }
			$product = Product::where('id',$request->id)
					->update($data);
            if ($product) {
                return redirect('/admin/product')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Product::destroy($id);
        return response()->json(['status' => (bool) $c]);
    }
}
