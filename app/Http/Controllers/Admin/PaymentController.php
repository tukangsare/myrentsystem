<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $orders = Order::orderBy('code','ASC')->paginate(10);
        } else {
            $orders = Order::orderBy('code','ASC')->paginate(10);
        }
        return view('admin.payment.index',[
            'orders'     	=> $orders,
            'total_data'    => Order::count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order      = Order::findOrFail($id);
        if (!$order) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak Administtrator'
                ]);
        }
        return view('admin.payment.edit',[
            'order' => $order,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'payment_date' => $request->tanggal,
            'payment_status' => true
        ]);
        return redirect('/admin/payment')->with([
            'status'    => 'success',
            'msg'       => 'Selamat produk sudah dikembalikan'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
