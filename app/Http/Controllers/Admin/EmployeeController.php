<?php

namespace App\Http\Controllers\Admin;

use App\Employee;
use App\User;
use App\JobPosition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class EmployeeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $employees = Employee::where('nik','LIKE',"%$cari%")
                ->orWhere('name','LIKE',"%$cari%")
                ->orWhere('jk','LIKE',"%$cari%")
                ->orWhere('alamat','LIKE',"%$cari%")
                ->orderBy('nik','asc')
                ->paginate(10);
        } else {
            $employees = Employee::orderBy('nik','asc')->paginate(10);
        }
        $employees->load('user','job_position');
        return view('admin.employee.index',[
            'employees'     => $employees,
            'total_data'    => Employee::count(),
            'request'       => $request
        ]);
    }
    public function create()
    {
        $job_positions = JobPosition::all();
        return view('admin.employee.create',['job_positions' => $job_positions]);        
    }
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'nik'       => 'required|unique:employees',
            'name'      => 'required',
            'jk'        => 'required',
            'address'   => 'required',
            'job'       => 'required',
        ]);
        if (!$validate->fails()) {
            $save = new Employee([
                'nik'           => $request->nik,
                'name'          => $request->name,
                'jk'            => $request->jk,
                'alamat'        => $request->address,
            ]);
            $save->job_position()->associate($request->job);
            if ($save->save())
            {
                return redirect()->route('admin.employee')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }
    public function edit($id)
    {
        $employee       = Employee::find($id);
        $job_positions  = JobPosition::all();
        return view('admin.employee.edit',[
            'employee'      => $employee,
            'job_positions' => $job_positions
        ]);        
    }
    public function update(Request $request,$id)
    {
        $employee = Employee::find($id);
        $validate = Validator::make($request->all(),[
            'name'      => 'required',
            'jk'        => 'required',
            'address'   => 'required',
            'job'       => 'required',
        ]);
        if (!$validate->fails()) {
            $employee->update([
                'name'          => $request->name,
                'jk'            => $request->jk,
                'alamat'        => $request->address,
            ]);
            $employee->job_position()->associate($request->job);
            if ($employee->save())
            {
                return redirect()->route('admin.employee')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }
    public function destroy($id)
    {
        $employee = Employee::destroy($id);
        return response()->json(['status' => (bool) $employee]);
    }
    public function setUserView($employee)
    {
        $users      = User::has('employees','<','1')->get();
        return view('admin.employee.set_user',['users' => $users,'id' => $employee]);        
    }
    public function setuser(Request $request,Employee $employee)
    {
        $employee->user()->associate($request->user);
        if($employee->save())
        {
            return redirect()->route('admin.employee')->with([
                'status'    => 'success',
                'msg'       => 'Selamat, User berhasil ditautkan'
            ]);
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }
}
