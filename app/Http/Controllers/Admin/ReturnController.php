<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\ReturnOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $orderReturn = Order::with('returns')->paginate(10);
        } else {
            $orderReturn = Order::with('returns')->paginate(10);
        }
        return view('admin.return.index',[
            'orders'         => $orderReturn,
            'total_data'    => Order::count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order      = Order::findOrFail($id);
        if (!$order) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak Administtrator'
                ]);
        }
        return view('admin.return.edit',[
            'order' => $order,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $returnOrder = new ReturnOrder();
        $returnOrder->return_date   = $request->tanggal;
        $returnOrder->desc   = $request->desc;
//        $returnOrder->save();
        $order->returns()->save($returnOrder);
//        $returnOrder->orders()->associate($order)->save();
        return redirect('/admin/return')->with([
            'status'    => 'success',
            'msg'       => 'Selamat pembayaran berhasil dilakukan'
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
