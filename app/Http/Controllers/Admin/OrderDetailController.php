<?php

namespace App\Http\Controllers\Admin;

use App\ProductDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDetail;
class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$id)
    {
        if ($request->cari) {
            $cari = $request->cari;
        } else {
            $p = Order::where('id',$id)->with('products')->get();
        }
        // return response()->json($p);
        return view('admin.order.detail.index',[
            'order_id'      => $id,
            'products'      => $p,
            'total_data'    => OrderDetail::where('order_id',$id)->count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_details = ProductDetail::with('products')->get();
        return view('admin.order.detail.create',['products' => $product_details]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $order = Order::find($id);
        $productDetail = ProductDetail::find($id)->products;
        $updatedTotalPrice = $order->total_price + $productDetail->price;
//        $order->update(['total_price' => $updatedTotalPrice]);
        $order->products()->attach($request->id);
        if ($order->update(['total_price' => $updatedTotalPrice])) {
            return redirect('/admin/o/'.$id.'/detail')->with([
                'status'    => 'success',
                'msg'       => 'Selamat Data berhasil dibuat'
            ]);
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($order_id, $id)
    {
        $order = Order::find($order_id);
//        $destroyDetail = ProductDetail::destroy($id);
        $order->products()->detach($id);
        $productDetail = ProductDetail::find($id)->products;
        $updatedTotalPrice = $order->total_price - $productDetail->price;
        return response()->json(['status' =>
            (bool) $order->update([
            'total_price' => $updatedTotalPrice
            ])
        ]);
    }
}
