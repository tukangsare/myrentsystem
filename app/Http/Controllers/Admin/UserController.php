<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $users = User::where('name','LIKE',"%$cari%")
                ->orWhere('username','LIKE',"%$cari%")
                ->orWhere('email','LIKE',"%$cari%")
                ->orderBy('name','asc')
                ->paginate(10);
        } else {
            $users = User::orderBy('name','asc')->paginate(10);
        }
        $users->load('roles');
        return view('admin.user.index',[
            'users'         => $users,
            'total_data'    => User::count(),
            'request'       => $request
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level = Role::all();
        return view('admin.user.create',['level' => $level]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required',
            'password'  => 'required',
            'level'     => 'required',
        ]);
        if (!$validate->fails()) {
            $level = Role::find($request->level);
            $save = $level->users()->create([
                'name'          => $request->name,
                'username'      => $request->username,
                'email'         => $request->email,
                'password'      => Hash::make($request->password),
            ]);
            if ($save)
            {
//                $save->roles()->associate(Role::find($request->level));
                return redirect()->route('admin.user')->with([
                    'status'    => 'success',
                    'msg'       => 'Selamat Data berhasil dibuat'
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user   = User::findOrFail($id);
        $level  = Role::all();
        if (!$user) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak administrator'
                ]);
        }
        return view('admin.user.edit',['user' => $user,'level' => $level]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        return $request->all();
        $validate = Validator::make($request->all(),[
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required',
            'level'     => 'required',
        ]);
        if (!$validate->fails()) {
            $user = User::find($id);
            $save = $user->update([
                'name'          => $request->name,
                'username'      => $request->username,
                'email'         => $request->email,
            ]);
            $user->roles()->associate($request->level);
            $user->save();
            if ($save)
            {
                return redirect()->route('admin.user')->with([
                    'status'    => 'success',
                    'msg'       => 'Selamat Data berhasil dibuat'
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }
    public function changeStatus(Request $request,User $user)
    {
        $msg = [];
        switch ($request->stat)
        {
            case '1':
                $user->is_active = 1;
                $user->save();
                $msg = [
                    'status'    => 'success',
                    'msg'       => 'Selamat User Aktif'
                ];
                break;
            case '0':
                $user->is_active = 0;
                $user->save();
                $msg = [
                    'status'    => 'success',
                    'msg'       => 'Selamat User Nonaktif'
                ];
                break;
            default:
                return back()
                    ->with('warning','Sepertinya ada kesalahan terjadi');
                break;
        }
        return back()
            ->with($msg);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        return response()->json(['status' => (bool) $user]);

    }
}
