<?php

namespace App\Http\Controllers\Admin;
use App\ProductDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Order;
use Illuminate\Support\Facades\DB;
use Validator;
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $orders = Order::where('code','LIKE',"%$cari%")
                ->orWhere('start_date','LIKE',"%$cari%")
                ->orWhere('end_date','LIKE',"%$cari%")
				->orWhere('total_price','LIKE',"%$cari%")
				->orWhereHas('customers',function ($query) use ($cari)
				{
					$query->where('name','LIKE',"%$cari%");
				})
                ->orderBy('code','asc')
                ->paginate(10);
		} else 
		{
            $orders = Order::with('customers')->orderBy('code','asc')->paginate(10);
		}
        return view('admin.order.index',[
            'orders'     	=> $orders,
            'total_data'    => Order::count(),
            'request'       => $request
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$detail_products = ProductDetail::with('products')->get();
		$customers = Customer::all();
		return view('admin.order.create',[
			'customers' => $customers,
			'detail_products' => $detail_products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        Cek jika ada produk yang sama
        if (count($request->product) !== count(array_unique($request->product))) {
            return back()
                ->with('warning','Data produk ada yang sama');
		}
        $validate = Validator::make($request->all(),
        [
            'code'      => 'required',
            'customer'  => 'required',
            'start_date'=> 'required',
            'end_date'  => 'required'
        ]);
		if (!$validate->fails()) {
		    $order = new Order([
		        'code' => $request->code,
                'order_date' => $request->start_date,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
            ]);
		    $order->customers()->associate($request->customer);
		    if ($order->save()) {
            $order->products()->sync($request->product);
            $prod_id = ProductDetail::select('product_id')
                ->whereIn('id',$request->product)
                ->get();
            $total_price = DB::table('products')
                ->select(DB::raw('SUM(price) as total_price'))
                ->whereIn('id', $request->product)
                ->get()->first();
		        $order->update(['total_price'=>$total_price->total_price]);
                return redirect()->route('admin.order.doc.create', ['order' => $order->id])->with([
                    'status'    => 'success',
                    'msg'       => 'Selamat Data berhasil dibuat'
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan');
	}
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order      = Order::findOrFail($id);
        $customers  = Customer::all();
        if (!$order) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak Administtrator' 
                ]);
        }
        return view('admin.order.edit',[
            'order' => $order,
            'customers' => $customers,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $findOrder = Order::findOrFail($id);
        $validate = Validator::make($request->all(),
        [
            'code'      => 'required',
            'customer'  => 'required',
            'start_date'=> 'required',
            'end_date'  => 'required'
        ]);
        if (!$validate->fails()) {
            $order = $findOrder->update([
                'code' => $request->code,
                'start_date' => $request->start_date,
                'end_date' => $request->end_date,
            ]);
            $findOrder->customers()->associate($request->customer);
            if ($order) {
                return redirect('/admin/order')->with([
                    'status'    => 'success',
                    'msg'       => 'Selamat Data berhasil dibuat'
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $orderDestroy = Order::destroy($id);
        return response()->json(['status' => (bool) $orderDestroy]);
    }
}
