<?php

namespace App\Http\Controllers\Admin;

use App\OrderDocument;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderDocumentSetting;
class DocumentOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Order $order)
    {
        $doc_order = OrderDocumentSetting::with([ 'orders' => function($q) use($order){
            return $q->where('orders.id','=',$order->id);
        }])->get();
        if ($request->cari) {
            $cari = $request->cari;
        } else {
//            $p = Order::where('id',$order)->get();
        }
//        return response()->json($doc_order);
        return view('admin.order.doc.index',[
            'order'         => $order,
            'doc_order'     => $doc_order,
            'total_data'    => $order->doc_order->count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Order $order)
    {
        $doc_order = OrderDocumentSetting::all();
        return view('admin.order.doc.create',[
            'order'     => $order,
            'doc_order' => $doc_order
        ]);
    }
    public function createOne(Order $order,OrderDocumentSetting $doc_setting)
    {
        return view('admin.order.doc.create-one',[
            'order' => $order,
            'doc_setting'   => $doc_setting
        ]);
    }
    public function storeOne(Order $order,OrderDocumentSetting $doc_setting,Request $request)
    {
        $uploaded_file = $request->file('name');
        if ($uploaded_file)
        {
            $file_ext = '.'.$uploaded_file->getClientOriginalExtension();
            if ($file_ext == $doc_setting->document_file_type)
            {
                $this->storagePath  = $doc_setting->document_storage_path;
                $this->image_prefix = 'bukti';
                $saveImage = $this->saveImage($request,'name');
                if ($saveImage)
                {
                    $order->doc_order()->attach($doc_setting,[
                        'document_name' => $saveImage,
                        'is_accepted'   => '0'
                    ]);
                    return redirect()->route('admin.order.doc',['order' => $order->id]);
                }
            }
        }
        return back()->with('warning','Sepertinya ada kesalahan terjadi. Mungkin eksistensi yang di upload tidak sesuai, silahkan coba lagi');
    }
    public function acc(Order $order,OrderDocumentSetting $doc_setting,Request $request)
    {
        $doc_order = $doc_setting->orders()->first();
        $doc_order->pivot->is_accepted = '1';
        if ($doc_order->pivot->save())
        {
            return back();
        } else{
            return back()->with('warning','Hmmm, sepertinya ada kesalahan');
        }
    }
    public function download(Order $order,OrderDocumentSetting $doc_setting)
    {
        $data = \DB::table('order_documents')->select('*')->where(['order_id' => $order->id,'order_document_setting_id' => $doc_setting->id])->first();
        return \Storage::disk('public')->download($data->document_name);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Order $order,Request $request)
    {
        $newFile = [];
        foreach ($_FILES as $key => $value) 
        {
            $doc_setting = OrderDocumentSetting::find(explode('_', $key)[1]);

            $uploadFile = $request->file($key);
            
            $file_ext  = $uploadFile->getClientOriginalExtension();
            if ('.'.$file_ext == $doc_setting->document_file_type)
            {
                $this->storagePath  = $doc_setting->document_storage_path;
                $this->image_prefix = 'bukti';
                $saveImage = $this->saveImage($request,$key);
                $order->doc_order()->attach($doc_setting,[
                    'document_name' => $saveImage,
                    'is_accepted'   => '0'
                ]);
                if (!$saveImage) 
                {
                    return back();
                }
            }
        }
        return redirect()->route('admin.order');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
