<?php

namespace App\Http\Controllers\Admin;

use App\OrderDocumentSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DocumentOrderSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $doc_order_setting = OrderDocumentSetting::where('document_code','LIKE',"%$cari%")
                ->orWhere('field_name','LIKE',"%$cari%")
                ->orderBy('document_code','asc')
                ->paginate(10);
        } else {
            $doc_order_setting = OrderDocumentSetting::orderBy('document_code','asc')->paginate(10);
        }
        return view('admin.setting.doc-order.index',[
            'doc_order_settings'     => $doc_order_setting,
            'total_data'            => OrderDocumentSetting::count(),
            'request'               => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.setting.doc-order.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'code' => 'required',
            'name' => 'required',
            'path' => 'required',
            'file_type' => 'required',
            'is_required' => 'required',
        ]);
        if (!$validate->fails()) {
            $save = OrderDocumentSetting::create([
                'document_code'         => $request->code,
                'field_name'            => $request->name,
                'document_storage_path' => $request->path,
                'document_file_type'    => $request->file_type,
                'is_required'           => $request->is_required,
            ]);
            if ($save)
            {
                return redirect()->route('admin.setting.doc-order')->with([
                    'status'    => 'success',
                    'msg'       => 'Selamat Data berhasil dibuat'
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doc_order = OrderDocumentSetting::findOrFail($id);
        if (!$doc_order) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak administrator'
                ]);
        }
        return view('admin.setting.doc-order.edit',['doc_order' => $doc_order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required',
            'path' => 'required',
            'file_type' => 'required',
            'is_required' => 'required',
        ]);
        if (!$validate->fails()) {
            $save = OrderDocumentSetting::where('id',$id)
                ->update([
                    'field_name'            => $request->name,
                    'document_storage_path' => $request->path,
                    'document_file_type'    => $request->file_type,
                    'is_required'           => $request->is_required,
                ]);
            if ($save) {
                return redirect()->route('admin.setting.doc-order')->with([
                    'status'    => 'success',
                    'msg'       => "<strong>Selamat!</strong> Data berhasil diupdate"
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $doc_order = OrderDocumentSetting::destroy($id);
        return response()->json(['status' => (bool) $doc_order]);
    }
}
