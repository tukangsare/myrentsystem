<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use KodeGen;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->cari) {
            $cari = $request->cari;
            $customers = Customer::where('name','LIKE',"%$cari%")
                ->orWhere('phone','LIKE',"%$cari%")
                ->orWhere('email','LIKE',"%$cari%")
                ->orWhere('address','LIKE',"%$cari%")
                ->orderBy('name','asc')
                ->paginate(10);
        } else {
            $customers = Customer::orderBy('name','asc')->paginate(10);
        }
        return view('admin.customer.index',[
            'customers'     => $customers,
            'total_data'    => Customer::count(),
            'request'       => $request
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);
        if (!$validate->fails()) {
            $save = Customer::create([
                'code'          => KodeGen::auto('customers', 'id', 'C-'),
                'name'          => $request->name,
                'phone'         => $request->phone,
                'email'         => $request->email,
                'address'       => $request->address,
                'description'   => $request->description
            ]);
            if ($save)
            {
                return redirect('/admin/customer')->with([
                        'status'    => 'success',
                        'msg'       => 'Selamat Data berhasil dibuat'
                    ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);
        if (!$customer) {
            return back()
                ->with([
                    'status'    => 'warning',
                    'msg'       => 'Error telah ditemukan, silahkan kontak administrator'
                ]);
        }
        return view('admin.customer.edit',['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),[
            'name' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);
        if (!$validate->fails()) {
            $save = Customer::where('id',$id)
                        ->update([
                                'name'          => $request->name,
                                'phone'         => $request->phone,
                                'email'         => $request->email,
                                'address'       => $request->address,
                                'description'   => $request->description
                            ]);
            if ($save) {
                return redirect('/admin/customer')->with([
                    'status'    => 'success',
                    'msg'       => "<strong>Selamat!</strong> Data berhasil diupdate"
                ]);
            }
        }
        return back()
            ->with('warning','Sepertinya ada kesalahan terjadi');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Customer::destroy($id);
        return response()->json(['status' => (bool) $c]);
    }
//    public function cari(Request $request) {
//
//    }
}
