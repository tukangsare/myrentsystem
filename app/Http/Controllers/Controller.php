<?php

namespace App\Http\Controllers;

use App\Helpers\StringGenerator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $storagePath;
    protected $image_prefix;

    protected function saveImage($request, $string)
    {
        $thumbnail = $request->file($string);
        if(!$thumbnail)
        {
            return FALSE;
        }
        $extension = $thumbnail->getClientOriginalExtension();
        $fileName       = $this->storagePath.StringGenerator::generate($this->image_prefix).'.'.$extension;
        if (\Storage::disk('public')->put($fileName,  \File::get($thumbnail)))
        {
            return $fileName;
        }
        return FALSE;
    }

}
