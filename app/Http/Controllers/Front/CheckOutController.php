<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CheckOutController extends Controller
{
    public function index()
    {
        if(Auth::user())
        {
            return view('front.item.desc',compact('product'));
        } else
        {
            return view('front.login');
        }
    }
}
