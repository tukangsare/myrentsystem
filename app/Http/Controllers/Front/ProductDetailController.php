<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductDetailController extends Controller
{
    public function index($id)
    {
        $product = Product::find($id);
        return view('front.item.desc',compact('product'));
    }
}
