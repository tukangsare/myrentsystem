<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index()
    {
        return view('front.login');
    }
    public function validateLogin(Request $request)
    {
        return $request->all();
    }
}