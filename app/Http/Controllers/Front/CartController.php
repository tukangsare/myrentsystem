<?php

namespace App\Http\Controllers\Front;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index()
    {
        $carts = session('shopcart.prod');
        return view('front.cart',compact('carts'));
    }
    public function addProduct(Product $prod,Request $request)
    {
        $shopcartprod = session('shopcart.prod')??[];
        $search = array_search($prod->id,array_column($shopcartprod,'id'));
//        return $search;
        if (!is_int($search))
        {
            session()->push('shopcart.prod', [
                'id' => $prod->id,
                'code' => $prod->code,
                'name' => $prod->name,
                'price' => $prod->getOriginal('price'),
                'image' => $prod->images,
                'qty' => 1
            ]);
        } else
        {
            session()->put('shopcart.prod.'.$search,[
                'id' => $prod->id,
                'code' => $prod->code,
                'name' => $prod->name,
                'price' => $prod->getOriginal('price'),
                'image' => $prod->images,
                'qty' => $shopcartprod[$search]['qty']+1
            ]);
        }
        return back();
    }
}
