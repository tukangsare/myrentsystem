<?php

namespace App\Http\Controllers\Front;

use App\Product;
use App\ProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    public function index()
    {
        $cats = ProductType::all();
        $product = Product::paginate(12);
        return view('front.shop',compact('cats','product'));
    }
    public function indexByCategory(ProductType $cat)
    {
        $cats = ProductType::all();
        $product = Product::where('product_type_id','=',$cat->id)->paginate(12);
        return view('front.shop',compact('cats','product'));
    }
}
