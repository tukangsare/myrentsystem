<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'code','name','product_type_id','images','price'
	];
    public function product_types()
    {
        return $this->belongsTo('App\ProductType','product_type_id');
    }
    public function product_details()
    {
        return $this->hasMany('App\ProductDetail');
	}

	public function getPriceAttribute($price)
	{
		return "Rp. ".number_format($price,2,',','.');
	}
}
