<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'nik', 'name', 'jk', 'alamat', 'job_position_id', 'user_id'
    ];
    public function job_position()
    {
        return $this->belongsTo(\App\JobPosition::class,'job_position_id');
    }
    public function user()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }
}
