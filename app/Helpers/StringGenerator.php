<?php
/**
 * Created by PhpStorm.
 * User: koderliar
 * Date: 1/18/19
 * Time: 1:13 PM
 */

namespace App\Helpers;


class StringGenerator
{
    static function generate($prefix = '')
    {
        return sprintf("%s_%s",$prefix,time());
    }
}