<?php
/**
 * Created by PhpStorm.
 * User: sampurasun
 * Date: 03/01/18
 * Time: 11:38
 */

namespace App\Helpers;


use DB;

class AutoNumber
{
    public static function auto($table, $id, $prefix)
    {
        $query = DB::table($table)->select(DB::raw("MAX(RIGHT(".$id.",4)) as max_id"))->get();
        if (!$query->count() > 0)
        {
            return $prefix.'00001';
        }
        foreach($query as $hasil)
        {
            $tmp = ((int) $hasil->max_id)+1;
            $kode = $prefix.sprintf("%05s",$tmp);
        }
        return $kode;
	}
	public static function rp($cost)
	{
		return "Rp. ".number_format($cost,2,',','.');
	}
}