<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDocument extends Model
{
    protected $fillable = [
        'document_name','is_accepted',
    ];
    public function orders()
    {
        return $this->belongsTo('App\Order');
    }
    public function docsetting()
    {
        return $this->belongsTo('App\OrderDocumentSetting');
    }
}
