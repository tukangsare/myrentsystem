<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
	protected $fillable = [
		'product_id', 'product_detail_code', 'engine_code', 'nopol','product_images','color','desc'
	];
    public function products()
    {
        return $this->belongsTo('App\Product','product_id');
	}
	public function orders()
	{
        return $this->belongsToMany('App\Order','order_details')->using('App\OrderDetail');
	}
}
