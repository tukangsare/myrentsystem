<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReturnOrder extends Model
{
    protected $table = 'returns';
    protected $fillable = [
        'order_id','return_date','description'
    ];
    public function orders() {
        return $this->belongsTo('App\Order','order_id');
    }
}
