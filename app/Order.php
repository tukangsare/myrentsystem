<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
		'code','order_date','start_date','end_date','payment_status','total_price'
	];
	public function customers()
	{
		return $this->belongsTo('App\Customer','customer_id');
	}
    public function products()
    {
        return $this->belongsToMany('App\ProductDetail','order_details')->withPivot('id');
    }
    public function doc_order()
    {
        return $this->belongsToMany('App\OrderDocumentSetting','order_documents')->withPivot(['id','document_name','is_accepted']);
    }
	public function returns()
    {
	    return $this->  hasOne('App\ReturnOrder');
    }
	public function getPaymentStatusAttribute($payment_status)
	{
		return ($payment_status == false)?'Belum Dibayar':'Sudah Dibayar';
	}
}
