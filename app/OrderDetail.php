<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class OrderDetail extends Pivot
{
    protected $table = 'order_details';
    protected $fillable = [
		'order_id','product_detail_id'
	];
    public function orders()
    {
        return $this->hasMany('App\Order');
    }
    public function products()
    {
        return $this->hasMany('App\Product');
    }
}
