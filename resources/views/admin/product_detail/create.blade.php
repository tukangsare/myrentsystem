@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-2 col-xs-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Add Product Detail
					</div>
					<div class="panel-body">
						@if(session('warning'))
							<div class="alert alert-danger">
								{{session('warning')}}
							</div>
						@endif
						<form action="{{url('/admin/p/'.Request::segment(3).'/detail/create')}}" method="POST" role="form" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-group">
								<label for="code">Kode</label>
								<input type="text" class="form-control" name="code" placeholder="Masukkan Kode">
							</div>
							<div class="form-group">
								<label for="engine_code">No Mesin</label>
								<input type="text" class="form-control" name="engine_code" placeholder="Masukkan No Mesin">
							</div>
							<div class="form-group">
								<label for="color">Warna</label>
								<input type="text" class="form-control" name="color" placeholder="Masukkan Warna">
							</div>
							<div class="form-group">
								<label for="nopol">Nopol</label>
								<input type="text" class="form-control" name="nopol" placeholder="Masukkan Nopol">
							</div>
							<div class="form-group">
								<label for="desc">Desc *</label>
								<textarea class="form-control" name="desc" placeholder="Masukkan Deskripis"></textarea>
								<span>(*) <i>Opsional</i></span>
							</div>
							<div class="form-group">
								<label for="nopol">Gambar *</label>
								<input type="file" class="form-control" name="product_image">
								<span>(*) <i>Opsional</i></span>
							</div>
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-success">Tambah</button>
								<button id="kembali" class="btn btn-default">Kembali</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')
	<script>
		$(document).ready(function () {
		   $('#kembali').on('click',function (e) {
			  e.preventDefault();
			  window.location.href="{{url('/admin/p/'.Request::segment(3).'/detail')}}";
		   });
		});
	</script>
@endsection