@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Detail Produk
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
							<div class="col-xs-12">
								@foreach($products as $p)
								<div class="col-xs-2">Kode Produk <span class="pull-right">:</span></div>
								<div class="col-xs-4">{{$p->code}}</div>
								<div class="col-xs-2">Nama Produk <span class="pull-right">:</span></div>
								<div class="col-xs-4">{{$p->name}}</div>
								<div class="col-xs-2">Harga Produk <span class="pull-right">:</span></div>
								<div class="col-xs-4">{{$p->price}}/hari</div>
								@endforeach
								<br>
							</div>
							<hr>
							<hr>
                            <div class="col-xs-2">
                                <a href="{{url('/admin/p/'.$product_id.'/detail/create')}}" class="btn btn-sm btn-success">Tambah</a>
                            </div>
                            <div class="col-xs-offset-6 col-xs-4">
                                <form action="{{url('/admin/'.$product_id.'/detail/create')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
							
                            <div class="col-xs-12">
                            <br>
							    <table class="table table-responsive">
                                    <thead>
                                    <th>Kode Produk</th>
                                    <th>No Mesin</th>
                                    <th>Warna</th>
                                    <th>Nopol</th>
                                    <th>Deskripsi</th>
                                    <th class="text-center">Aksi</th>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)
										@if($product->product_details->isEmpty())
                                        <tr>
                                            <td class="text-center" colspan="7">Data Tidak Ditemukan</td>
                                        </tr>
                                    	@else
										@foreach($product->product_details as $d_product)
                                            <tr>
                                                <td>{{$d_product->product_detail_code}}</td>
                                                <td>{{$d_product->engine_code}}</td>
                                                <td>{{$d_product->color}}</td>
                                                <td>{{$d_product->nopol}}</td>
                                                <td>{{$d_product->desc}}</td>
                                                <td class="text-center col-xs-2">
												<a href="{{url('/admin/p/'.$product_id.'/detail/'.$d_product->id.'/edit')}}" class="btn btn-warning">Edit</a>
												<a href="" id="hapus" class="btn btn-danger" data-token="{{csrf_token()}}" data-id="{{$d_product->id}}">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
										@endif
                                        @endforeach
                                    
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}} </strong>
                            </span>
                            <div class="paging pull-right">
                                <!-- {{$products->appends($request->only('cari'))->links()}} -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#hapus').on('click',function (e) {
                var id = $(this).data('id');
				var prod_id = {{$product_id}};
                var token = $(this).data('token');
                e.preventDefault();
                $.ajax({
                    url:"/admin/p/"+prod_id+"/detail/"+id+"/delete",
                    method:'DELETE',
                    dataType: 'json',
                    data: {
                        "_method":'DELETE',
                        "_token":token
                    }
                }).done(function (data) {
                    if (data.status == true)
                    {
                        alert('Data Berhasil dihapus');
                        window.location.reload(true);
                        return true;
                    }
                    alert('Data Gagal Dihapus bang');
                    return false;
                }).fail(function () {
                    alert('Data Gagal Dihapus');
                })
            });
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection