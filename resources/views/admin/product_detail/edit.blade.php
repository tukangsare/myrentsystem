@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-2 col-xs-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Edit Product Detail
					</div>
					<div class="panel-body">
						@if(session('warning'))
							<div class="alert alert-danger">
								{{session('warning')}}
							</div>
						@endif
						<form action="{{url('/admin/p/'.Request::segment(3).'/detail/'.Request::segment(5).'/edit')}}" method="POST" role="form" enctype="multipart/form-data">
							<input type="hidden" name="_method" value="PATCH">
							{{csrf_field()}}
							<div class="form-group">
								<label for="code">Kode</label>
								<input type="text" class="form-control" name="code" placeholder="Masukkan Kode" value="{{$detail->product_detail_code}}" disabled>
							</div>
							<div class="form-group">
								<label for="engine_code">No Mesin</label>
								<input type="text" class="form-control" name="engine_code" placeholder="Masukkan No Mesin" value="{{$detail->engine_code}}">
							</div>
							<div class="form-group">
								<label for="color">Warna</label>
								<input type="text" class="form-control" name="color" placeholder="Masukkan Warna" value="{{$detail->color}}">
							</div>
							<div class="form-group">
								<label for="nopol">Nopol</label>
								<input type="text" class="form-control" name="nopol" placeholder="Masukkan Nopol" value="{{$detail->nopol}}">
							</div>
							<div class="form-group">
								<label for="desc">Desc *</label>
								<textarea class="form-control" name="desc" placeholder="Masukkan Deskripis">{{$detail->desc}}</textarea>
								<span>(*) <i>Opsional</i></span>
							</div>
							<div class="form-group">
								<label for="nopol">Gambar *</label>
								<input type="file" class="form-control" name="product_image">
								<span>(*) <i>Opsional</i></span>
							</div>
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-success">Edit</button>
								<button id="kembali" class="btn btn-default">Kembali</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')
	<script>
		$(document).ready(function () {
		   $('#kembali').on('click',function (e) {
			  e.preventDefault();
			  window.location.href="{{url('/admin/p/'.Request::segment(3).'/detail')}}";
		   });
		});
	</script>
@endsection