@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Employees
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-2">
                                <a href="{{route('admin.employee.create')}}" class="btn btn-sm btn-success">Tambah</a>
                            </div>
                            <div class="col-xs-offset-6 col-xs-4">
                                <form action="{{route('admin.employee')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-responsive">
                                    <thead>
                                        <tr>
                                            <th>Nik</th>
                                            <th>Nama</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Jabatan</th>
                                            <th>User</th>
                                            <th>Alamat</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if($employees->isEmpty())
                                        <tr>
                                            <td class="text-center" colspan="5">Data Tidak Ditemukan</td>
                                        </tr>
                                    @else
                                        @foreach($employees as $employee)
                                            <tr>
                                                <td>{{$employee->nik}}</td>
                                                <td>{{$employee->name}}</td>
                                                <td>{{$employee->jk}}</td>
                                                <td>{!!($employee->job_position)?sprintf("%s",$employee->job_position->job_name):'-'!!}</td>
                                                <td>{{($employee->user)?$employee->user->username:'Belum terhubung'}}</td>
                                                <td>{{$employee->alamat}}</td>
                                                <td>
                                                    <table width="100%">
                                                        <tr>
                                                            <td width="33%" class="text-center">
                                                                <a href="{{url('/admin/employee/'.$employee->id.'/set_user')}}" class="btn btn-sm btn-default">Set User</a>
                                                            </td>
                                                            <td width="33%" class="text-center">
                                                                <a href="{{url('/admin/employee/'.$employee->id.'/edit')}}" class="btn btn-sm btn-warning">Edit</a>
                                                            </td>
                                                            <td width="33%" class="text-center">
                                                                <a href="" id="hapus" class="btn btn-sm btn-danger" data-token="{{csrf_token()}}" data-id="{{$employee->id}}">Hapus</a>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}}</strong>
                            </span>
                            <div class="paging pull-right">
                                {{$employees->appends($request->only('cari'))->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#hapus').on('click',function (e) {
                var id = $(this).data('id');
                var token = $(this).data('token');
                e.preventDefault();
                $.ajax({
                    url:"/admin/employee/"+id+"/delete",
                    method:'DELETE',
                    dataType: 'json',
                    data: {
                        "_method":'DELETE',
                        "_token":token
                    }
                }).done(function (data) {
                    if (data.status == true)
                    {
                        alert('Data Berhasil dihapus');
                        window.location.reload(true);
                        return true;
                    }
                    alert('Data Gagal Dihapus');
                    return false;
                }).fail(function () {
                    alert('Data Gagal Dihapus');
                })
            });
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection