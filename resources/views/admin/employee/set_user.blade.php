@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Set User for Employee
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{route('admin.employee.set_user.post',['employee' => $id])}}" method="POST" role="form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="user">User</label>
                                <select class="form-control" name="user" required>
                                @foreach($users AS $user)
                                    <option value="{{$user->id}}">{{$user->username}}</option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Atur</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
           $('#kembali').on('click',function (e) {
              e.preventDefault();
              window.location.href="{{route('admin.employee')}}";
           });
        });
    </script>
@endsection