@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Employee
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{route('admin.employee.update',['id' => $employee->id])}}" method="POST" role="form">
                            <input type="hidden" name="_method" value="PATCH">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="nik">Nik</label>
                                <input type="text" class="form-control" id="nik" name="nik" placeholder="Masukkan Nik" disabled value="{{$employee->nik}}">
                            </div>
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama" required value="{{$employee->name}}">
                            </div>
                            <div class="form-group">
                                <label for="jk">Jenis Kelamin</label>
                                <select class="form-control" name="jk" required>
                                    <option value="L" {{($employee->jk === 'L')?'selected':''}}>Laki-laki</option>
                                    <option value="P" {{($employee->jk === 'P')?'selected':''}}>Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="address">Alamat</label>
                                <textarea class="form-control" name="address" placeholder="Masukkan Alamat" required>{{$employee->alamat}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="job">Jabatan</label>
                                <select class="form-control" name="job" required>
                                    <option value="">Pilih Jabatan</option>
                                    @foreach($job_positions AS $job)
                                        <option value="{{$job->id}}" {{($employee->job_position_id === $job->id)?'selected':''}}>{{$job->job_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Edit</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
           $('#kembali').on('click',function (e) {
              e.preventDefault();
              window.location.href="{{route('admin.employee')}}";
           });
        });
    </script>
@endsection