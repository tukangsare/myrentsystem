@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Setting Document Order
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-2">
                                <a href="{{route('admin.setting.doc-order.create')}}" class="btn btn-sm btn-success">Tambah</a>
                            </div>
                            <div class="col-xs-offset-6 col-xs-4">
                                <form action="{{route('admin.setting.doc-order')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-responsive">
                                    <thead>
                                    <th>Kode Dokumen</th>
                                    <th>Nama Dokumen</th>
                                    <th>Tempat disimpan</th>
                                    <th>Tipe File</th>
                                    <th>Apakah Wajib Diisi</th>
                                    <th class="text-center">Aksi</th>
                                    </thead>
                                    <tbody>
                                    @if($doc_order_settings->isEmpty())
                                        <tr>
                                            <td class="text-center" colspan="6">Data Tidak Ditemukan</td>
                                        </tr>
                                    @else
                                        @foreach($doc_order_settings as $doc_order_setting)
                                            <tr>
                                                <td>{{$doc_order_setting->document_code}}</td>
                                                <td>{{$doc_order_setting->field_name}}</td>
                                                <td>{{$doc_order_setting->document_storage_path}}</td>
                                                <td>{{\App\OrderDocumentSetting::getExtFiles($doc_order_setting->document_file_type)}}</td>
                                                <td>{{$doc_order_setting->is_required}}</td>
                                                <td class="text-center">
                                                    <a href="{{url('/admin/setting/doc-order/'.$doc_order_setting->id.'/edit')}}" class="btn btn-warning">Edit</a>
                                                    <a href="#" class="hapus btn btn-danger" data-token="{{csrf_token()}}" data-id="{{$doc_order_setting->id}}">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}}</strong>
                            </span>
                            <div class="paging pull-right">
                                {{$doc_order_settings->appends($request->only('cari'))->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('.hapus').on('click',function (e) {
                var id = $(this).data('id');
                var token = $(this).data('token');
                e.preventDefault();
                $.ajax({
                    url:"/admin/setting/doc-order/"+id+"/delete",
                    method:'DELETE',
                    dataType: 'json',
                    data: {
                        "_method":'DELETE',
                        "_token":token
                    }
                }).done(function (data) {
                    if (data.status == true)
                    {
                        alert('Data Berhasil dihapus');
                        window.location.reload(true);
                        return true;
                    }
                    alert('Data Gagal Dihapus');
                    return false;
                }).fail(function () {
                    alert('Data Gagal Dihapus');
                })
            });
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection