@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Edit Document Order Setting
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{route('admin.setting.doc-order.update', ['id' => $doc_order->id])}}" method="POST" role="form">
                            <input type="hidden" name="_method" value="PATCH">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Kode</label>
                                <input type="text" class="form-control" name="code" placeholder="Masukkan Kode Dokumen"
                                       disabled value="{{$doc_order->document_code}}">
                            </div>
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Dokumen" required value="{{$doc_order->field_name}}">
                            </div>
                            <div class="form-group">
                                <label for="phone">Tempat Simpan Dokumen</label>
                                <input type="text" class="form-control" name="path" placeholder="Contoh bukti/ktp" required value="{{$doc_order->document_storage_path}}">
                                <small>Dipisah dengan tanda slah (/)</small>
                            </div>
                            <div class="form-group">
                                <label for="email">Tipe File</label>
                                <select class="form-control" name="file_type" required>
                                    @foreach(\App\OrderDocumentSetting::AVAILABLE_FILE_EXTS AS $ext => $placeholder)
                                        <option value="{{$ext}}" {{$doc_order->document_file_type==$ext?'selected':''}}>{{$placeholder}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Wajib Di isi</label>
                                <select class="form-control" name="is_required" required>
                                    <option value="1" {{$doc_order->getOriginal('is_required')=='1'?'selected':''}}>Ya</option>
                                    <option value="0" {{$doc_order->getOriginal('is_required')=='0'?'selected':''}}>Tidak</option>
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Edit</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{route('admin.setting.doc-order')}}";
            });
        });
    </script>
@endsection