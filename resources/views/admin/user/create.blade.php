@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add User
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{route('admin.user.create')}}" method="POST" role="form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama" required>
                            </div>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input type="text" class="form-control" name="username" placeholder="Masukkan Username" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Masukkan Email" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <div class="input-group" id="show-hide-password">
                                    <input type="password" class="form-control" name="password" placeholder="Masukkan Password" required>
                                    <div class="input-group-addon">
                                        <a href="">Show</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="level">Level</label>
                                <select name="level" id="" class="form-control">
                                    <option value="">-- Pilih Level --</option>
                                    @foreach($level AS $lvl)
                                        <option value="{{$lvl->id}}">{{$lvl->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{route('admin.user')}}";
            });
            $('#show-hide-password a').on('click', function (e) {
                e.preventDefault();
                console.log('Kepencet');
                if($('#show-hide-password input').attr('type') == 'text')
                {
                    $('#show-hide-password input').attr('type','password');
                    $('#show-hide-password a').html('Show');
                } else if($('#show-hide-password input').attr('type') == 'password')
                {
                    $('#show-hide-password input').attr('type','text');
                    $('#show-hide-password a').html('Hide');
                }
            })
        });
    </script>
@endsection