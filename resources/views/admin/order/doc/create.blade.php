@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="pull-left">
                            Add Document
                        </span>
                        <span class="pull-right">
                            Order: <b>{{$order->code}}</b>
                        </span>
                    <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{route('admin.order.doc.post',['order' => $order->id])}}" method="POST" role="form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            @foreach($doc_order as $val)
                                <div class="form-group">
                                    <label>
                                        {{sprintf('%s: %s',$val->document_code,$val->field_name)}}
                                        @if($val->getOriginal('is_required') === '1')
                                            <sup>(<i style="color: red">Wajib Di isi</i>)</sup>
                                        @else
                                            <sup>(Optional)</sup>
                                        @endif
                                    </label>
                                    <input type="file" name="doc_{{$val->id}}" class="form-control">
                                </div>
                            @endforeach
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{route('admin.order')}}";
            });
        });
    </script>
@endsection