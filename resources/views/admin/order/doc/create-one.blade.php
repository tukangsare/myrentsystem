@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="pull-right"><b>Upload Dokumen</b></span>
                        <span class="clearfix"></span>
                        <div>Order Code : <b>{{$order->code}}</b></div>
                        <div>Doc Code : <b>{{$doc_setting->document_code}}</b></div>
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{route('admin.order.doc.post.one',['order' => $order->id,'doc_setting' => $doc_setting->id])}}" method="POST" role="form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">File</label>
                                <input type="file" class="
                                form-control" name="name" required>
                                <small><b>Note:</b> Format yang diterima adalah <b>{{$doc_setting->document_file_type}}</b></small>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Simpan</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{route('admin.order')}}";
            });
        });
    </script>
@endsection