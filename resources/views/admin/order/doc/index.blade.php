@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Detail Produk</b>
                        <a href="{{route('admin.order')}}" class="btn btn-info pull-right">Kembali</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="col-xs-4">Kode Order</div>
                                <div class="col-xs-1">:</div>
                                <div class="col-xs-7"><b>{{$order->code}}</b></div>
                                <div class="col-xs-4">Customer</div>
                                <div class="col-xs-1">:</div>
                                <div class="col-xs-7"><b>{{ucwords($order->customers->getOriginal('name'))}}</b></div>
                            </div>
                            <div class="col-xs-4 pull-right">
                                <form action="{{url('/admin/'.$order->id.'/doc')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12">
                                <hr>
                            </div>

                            <div class="col-xs-12">
                                <br>
                                <table class="table table-responsive">
                                    <thead>
                                    <th>Kode Dokumen</th>
                                    <th>Status</th>
                                    <th class="text-center">Aksi</th>
                                    </thead>
                                    <tbody>
                                    @foreach($doc_order AS $doc)
                                        <tr>
                                            <td>{{$doc->document_code}}</td>
                                            @if($doc->orders->isEmpty())
                                                <td>
                                                    <span class="label label-info">Dokumen Belum diisi</span>
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{route('admin.order.doc.create.one',['order' => $order->id,'doc_setting' =>$doc->id])}}" class="btn btn-sm btn-success">Isi</a>
                                                </td>
                                            @else

                                                    @if($doc->orders[0]->pivot->is_accepted == '1')
                                                    <td>
                                                        <span class="label label-success">Diterima</span>
                                                    </td>
                                                    <td class="text-center">
                                                        -
                                                    </td>
                                                    @else
                                                    <td>
                                                        <span class="label label-warning">Belum Diterima</span>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="{{route('admin.order.doc.download',['order' => $order->id,'doc_setting' => $doc->id])}}" class="btn btn-sm btn-default">Lihat Doc</a>
                                                        <a href="{{route('admin.order.doc.acc',['order' => $order->id,'doc_setting' => $doc->id])}}" class="btn btn-sm btn-info">Terima</a>
                                                    </td>
                                                    @endif
                                            @endif
                                        </tr>
                                    @endforeach
                                    {{--@foreach($products as $product)--}}
                                        {{--@if($product->products->isEmpty())--}}
                                            {{--<tr>--}}
                                                {{--<td class="text-center" colspan="7">Data Tidak Ditemukan</td>--}}
                                            {{--</tr>--}}
                                        {{--@else--}}
                                            {{--@foreach($product->products as $d_product)--}}
                                                {{--<tr>--}}
                                                    {{--<td>{{$d_product->product_detail_code}}</td>--}}
                                                    {{--<td>{{$d_product->engine_code}}</td>--}}
                                                    {{--<td>{{$d_product->color}}</td>--}}
                                                    {{--<td>{{$d_product->nopol}}</td>--}}
                                                    {{--<td>{{$d_product->desc}}</td>--}}
                                                    {{--<td class="text-center col-xs-2">--}}
                                                        {{--<a href="" id="hapus" class="btn btn-danger" data-token="{{csrf_token()}}" data-id="{{$d_product->pivot->id}}">Hapus</a>--}}
                                                    {{--</td>--}}
                                                {{--</tr>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                    {{--@endforeach--}}

                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}} </strong>
                            </span>
                            <div class="paging pull-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#hapus').on('click',function (e) {
                var id = $(this).data('id');
                var prod_id = {{$order->id}};
                var token = $(this).data('token');
                e.preventDefault();
                $.ajax({
                    url:"/admin/o/"+prod_id+"/detail/"+id+"/delete",
                    method:'DELETE',
                    dataType: 'json',
                    data: {
                        "_method":'DELETE',
                        "_token":token
                    }
                }).done(function (data) {
                    if (data.status == true)
                    {
                        alert('Data Berhasil dihapus');
                        window.location.reload(true);
                        return true;
                    }
                    alert('Data Gagal Dihapus bang');
                    return false;
                }).fail(function () {
                    alert('Data Gagal Dihapus');
                })
            });
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection