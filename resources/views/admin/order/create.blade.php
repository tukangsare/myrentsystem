@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-2 col-xs-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Add Order
					</div>
					<div class="panel-body">
						@if(session('warning'))
							<div class="alert alert-danger">
								{{session('warning')}}
							</div>
						@endif
						<form action="{{url("/admin/order/create")}}" method="POST" role="form" id="formTambah">
							{{csrf_field()}}
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label for="code">Kode</label>
								<input type="text" class="form-control" name="code" placeholder="Masukkan Kode order">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="customer">Customer</label>
								<select name="customer" id="" class="form-control">
									<option value="">-- Pilih Customer --</option>
									@if($customers)
									@foreach($customers as $customer)
										<option value="{{$customer->id}}">{{$customer->name}}</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label for="start_date">Dari Tanggal</label>
								<input type="date" class="form-control" name="start_date" placeholder="Masukkan Tanggal Mulai Sewa">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="end_date">Sampai Tanggal</label>
								<input type="date" class="form-control" name="end_date" placeholder="Masukkan Tanggal Berakhir Sewa">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="">Produk</label>
					</div>
					<div id="product_add">
					<div class="row" id="field">
						<div id="field1">
						<div class="col-xs-10">
							<div class="form-group">
								<label for="product">Add</label>
								<select name="product[]" class="form-control">
									<option value="">-- Pilih Produk --</option>
									@foreach($detail_products as $detail_p)
									<option value="{{$detail_p->id}}">{{$detail_p->products->name}} - {{$detail_p->total}} - {{$detail_p->color}} - {{$detail_p->nopol}}</option>
									@endforeach
								</select>
							</div>
						</div>
						</div>
						<div class="col-xs-2">
							<div class="form-group">
								<label for="">&nbsp;</label>
								<button class="form-control btn btn-default add_product_button" id="add_product_button" name="add_product_button">Tambah</button>
							</div>
						</div>
					</div>
					</div>
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-success">Tambah</button>
								<button id="kembali" class="btn btn-default">Kembali</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')
	<script>
		$(document).ready(function () {
		   $('#kembali').on('click',function (e) {
			  e.preventDefault();
			  window.location.href="{{url('/admin/order')}}";
		   });
		   var next = 1;
		   $('.add_product_button').click(function(e) {
				e.preventDefault();
				var addTo = "#field"+next;
				var addRemove = "#field"+(next);
				next = next+1;
				var newIn = `<div id="field`+next+`">
						<div class="col-xs-10">
							<div class="form-group">
								<label for="product">Add</label>
								<select name="product[]" class="form-control">
										<option value="">-- Pilih Produk --</option>
										@foreach($detail_products as $detail_p)
										<option value="{{$detail_p->id}}">{{$detail_p->products->name}} - {{$detail_p->color}} - {{$detail_p->nopol}}</option>
										@endforeach
									</select>
								</div>
						</div>
						</div>
					</div>`;
				var newInput = $(newIn);
				var removeBtn = `<div class="form-group remove_me" id="remove_product_button_`+next+`">
									<div class="col-xs-2">
								<label for="">&nbsp;</label>
								<button class="form-control btn btn-default">Hapus</button>
							</div>
						</div>
						</div>`;
				var removeButton = $(removeBtn);
				$(addTo).after(newInput);
				$(addRemove).after(removeButton);
				$("#field"+next).attr('data-source',$(addTo).attr('data-source'));
				$('.remove_me').on('click',function (e){
					e.preventDefault();
					 // var fieldNum = this.id.charAt(this.id.length-1);
					var str = this.id.split("remove_product_button_");
					var fieldNum = str[1];
					var fieldID = "#field"+fieldNum;
					$(this).remove();
					$(fieldID).remove();
				})
		   });
           {{--$('#formTambah').on('submit',function (e) {--}}
			   {{--e.preventDefault();--}}
			   {{--var s = $(this).serializeArray();--}}
			   {{--$.ajax({--}}
				   {{--method: 'POST',--}}
				   {{--url: '{{url("/admin/order/create")}}',--}}
				   {{--dataType: 'json',--}}
				   {{--data: s--}}
			   {{--}).done(function (data) {--}}
				   {{--if (data.status) {--}}
				       {{--console.log(data.status);--}}
				   {{--} else {--}}
				       {{--console.log(data);--}}
				   {{--}--}}
               {{--});--}}
           {{--})--}}
		});
	</script>
@endsection