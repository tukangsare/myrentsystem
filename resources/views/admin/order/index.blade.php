@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Produk
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-2">
                                <a href="{{url('/admin/order/create')}}" class="btn btn-sm btn-success">Tambah</a>
                            </div>
                            <div class="col-xs-offset-6 col-xs-4">
                                <form action="{{url('/admin/order')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-responsive">
                                    <thead>
                                    <th>Kode</th>
                                    <th>Customer</th>
                                    <th>Tanggal Mulai Sewa</th>
                                    <th>Tanggal Selesai Sewa</th>
                                    <th>Status Bayar</th>
                                    <th>Total Biaya</th>
                                    <th class="text-center">Aksi</th>
                                    </thead>
                                    <tbody>
                                    @if(!$orders)
                                        <tr>
                                            <td class="text-center" colspan="2">Data Tidak Ditemukan</td>
                                        </tr>
                                    @else
                                        @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->code}}</td>
                                                <td>{{$order->customers->name}}</td>
                                                <td>{{$order->start_date}}</td>
                                                <td>{{$order->end_date}}</td>
                                                <td>{{$order->payment_status}}</td>
												<td>{{$order->total_price}}</td>
												<td class="text-center col-xs-3">
                                                <a href="{{route('admin.order.doc',['order'  => $order->id])}}" class="btn btn-sm btn-info">Doc</a>
                                                <a href="{{url('/admin/o/'.$order->id.'/detail')}}" class="btn btn-sm btn-info">Detail</a>
												<a href="{{url('/admin/order/'.$order->id.'/edit')}}" class="btn btn-sm btn-warning">Edit</a>
												<a href="" id="hapus" class="btn btn-sm .btn-danger" data-token="{{csrf_token()}}" data-id="{{$order->id}}">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}} </strong>
                            </span>
                            <div class="paging pull-right">
                                {{$orders->appends($request->only('cari'))->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#hapus').on('click',function (e) {
                var id = $(this).data('id');
                var token = $(this).data('token');
                e.preventDefault();
                $.ajax({
                    url:"/admin/order/"+id+"/delete",
                    method:'DELETE',
                    dataType: 'json',
                    data: {
                        "_method":'DELETE',
                        "_token":token
                    }
                }).done(function (data) {
                    if (data.status == true)
                    {
                        alert('Data Berhasil dihapus');
                        window.location.reload(true);
                        return true;
                    }
                    alert('Data Gagal Dihapus bang');
                    return false;
                }).fail(function () {
                    alert('Data Gagal Dihapus');
                })
            });
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection