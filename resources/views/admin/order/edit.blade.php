@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-2 col-xs-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Add Order
					</div>
					<div class="panel-body">
						@if(session('warning'))
							<div class="alert alert-danger">
								{{session('warning')}}
							</div>
						@endif
						<form action="{{url("/admin/order/$order->id/edit")}}" method="POST" role="form" id="formTambah">
                            <input type="hidden" name="_method" value="PATCH">
							{{csrf_field()}}
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label for="code">Kode</label>
								<input type="text" class="form-control" name="code" placeholder="Masukkan Kode order" value="{{$order->code}}">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="customer">Customer</label>
								<select name="customer" id="" class="form-control">
									<option value="">-- Pilih Customer --</option>
									@if($customers)
									@foreach($customers as $customer)
										<option value="{{$customer->id}}" {{($order->customers->id === $customer->id)?'selected':''}}>{{$customer->name}}</option>
									@endforeach
									@endif
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-6">
							<div class="form-group">
								<label for="start_date">Dari Tanggal</label>
								<input type="date" class="form-control" name="start_date" placeholder="Masukkan Tanggal Mulai Sewa" value="{{$order->start_date}}">
							</div>
						</div>
						<div class="col-xs-6">
							<div class="form-group">
								<label for="end_date">Sampai Tanggal</label>
								<input type="date" class="form-control" name="end_date" placeholder="Masukkan Tanggal Berakhir Sewa" value="{{$order->end_date}}">
							</div>
						</div>
					</div>
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-success">Edit</button>
								<button id="kembali" class="btn btn-default">Kembali</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')
	<script>
		$(document).ready(function () {
		   $('#kembali').on('click',function (e) {
			  e.preventDefault();
			  window.location.href="{{url('/admin/order')}}";
		   });
	</script>
@endsection