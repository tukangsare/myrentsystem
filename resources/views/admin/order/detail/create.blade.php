@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Product Detail
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{url('/admin/o/'.Request::segment(3).'/detail/create')}}" method="POST" role="form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="id">Produk</label>
                                <select name="id" class="form-control">
                                    <option value="">-- Pilihlah Produk --</option>
                                    @foreach($products as $product)
                                        <option value="{{$product->id}}">{{$product->products->name}} - {{$product->color}} - {{$product->nopol}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{url('/admin/o/'.Request::segment(3).'/detail')}}";
            });
        });
    </script>
@endsection