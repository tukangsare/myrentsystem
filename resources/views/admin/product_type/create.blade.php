@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Tambah Jenis Kendaraan
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{url('/admin/product-type/create')}}" method="POST" role="form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Jenis Kendaraan">
                            </div>
                            <div class="form-group">
                                <label for="thumbnail">Thumbnail</label>
                                <input type="file" class="form-control" name="thumbnail" placeholder="Masukkan Thumbnsil untuk Jenis Kendaraan">
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{url('/admin/product-type')}}";
            });
        });
    </script>
@endsection