@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Produk
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-2">
                                <a href="{{url('/admin/product/create')}}" class="btn btn-sm btn-success">Tambah</a>
                            </div>
                            <div class="col-xs-offset-6 col-xs-4">
                                <form action="{{url('/admin/product')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-responsive">
                                    <thead>
                                    <th>Kode</th>
                                    <th>Nama/Merek</th>
                                    <th>Jenis</th>
                                    <th>Gambar</th>
                                    <th>Harga</th>
                                    <th class="text-center">Aksi</th>
                                    </thead>
                                    <tbody>
                                    @if(!$types)
                                        <tr>
                                            <td class="text-center" colspan="2">Data Tidak Ditemukan</td>
                                        </tr>
                                    @else
                                        @foreach($types as $type)
                                            <tr>
                                                <td>{{$type->code}}</td>
                                                <td>{{$type->name}}</td>
                                                <td>{{$type->product_types->name}}</td>
                                                <td><img src="{{Storage::disk('public')->exists($type->images)?Storage::url($type->images):''}}" style="width: 50%;height: 50%" alt=""></td>
                                                <td>{{$type->price}}</td>
                                                <td class="text-center col-xs-3">
												<a href="{{url('/admin/p/'.$type->id.'/detail')}}" class="btn btn-info">Detail</a>
												<a href="{{url('/admin/product/'.$type->id.'/edit')}}" class="btn btn-warning">Edit</a>
												<a href="" id="hapus" class="btn btn-danger" data-token="{{csrf_token()}}" data-id="{{$type->id}}">Hapus</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}} </strong>
                            </span>
                            <div class="paging pull-right">
                                {{$types->appends($request->only('cari'))->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#hapus').on('click',function (e) {
                var id = $(this).data('id');
                var token = $(this).data('token');
                e.preventDefault();
                $.ajax({
                    url:"/admin/product/"+id+"/delete",
                    method:'DELETE',
                    dataType: 'json',
                    data: {
                        "_method":'DELETE',
                        "_token":token
                    }
                }).done(function (data) {
                    if (data.status == true)
                    {
                        alert('Data Berhasil dihapus');
                        window.location.reload(true);
                        return true;
                    }
                    alert('Data Gagal Dihapus bang');
                    return false;
                }).fail(function () {
                    alert('Data Gagal Dihapus');
                })
            });
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection