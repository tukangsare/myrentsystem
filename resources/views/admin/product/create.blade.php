@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-xs-offset-2 col-xs-8">
				<div class="panel panel-default">
					<div class="panel-heading">
						Add Product
					</div>
					<div class="panel-body">
						@if(session('warning'))
							<div class="alert alert-danger">
								{{session('warning')}}
							</div>
						@endif
						<form action="{{url('/admin/product/create')}}" method="POST" role="form" enctype="multipart/form-data">
							{{csrf_field()}}
							<div class="form-group">
								<label for="code">Kode</label>
								<input type="text" class="form-control" name="code" placeholder="Masukkan Kode Product">
							</div>
							<div class="form-group">
								<label for="name">Nama</label>
								<input type="text" class="form-control" name="name" placeholder="Masukkan Nama Product">
							</div>
							<div class="form-group">
								<label for="product_type">Jenis</label>
								<select name="product_type" id="" class="form-control">
									<option value="">-- Pilih Jenis Kendaraan --</option>
									@if($product_types)
									@foreach($product_types as $type)
										<option value="{{$type->id}}">{{$type->name}}</option>
									@endforeach
									@endif
								</select>
							</div>
							<div class="form-group">
								<label for="price">Harga</label>
								<input type="text" class="form-control" name="price" placeholder="Masukkan Harga">
								<span><span style="color:red">*</span><i>Harga per satu hari</i></span>
							</div>
							<div class="form-group">
								<label for="name">Gambar</label>
								<input type="file" class="form-control" name="image">
							</div>
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-success">Tambah</button>
								<button id="kembali" class="btn btn-default">Kembali</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('footer')
	<script>
		$(document).ready(function () {
		   $('#kembali').on('click',function (e) {
			  e.preventDefault();
			  window.location.href="{{url('/admin/product')}}";
		   });
		});
	</script>
@endsection