@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Add Customer
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{url('/admin/customer/create')}}" method="POST" role="form">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Customer" required>
                            </div>
                            <div class="form-group">
                                <label for="phone">No. Telp.</label>
                                <input type="text" class="form-control" name="phone" placeholder="Masukkan No. Telp. Customer" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Masukkan Email Customer" required>
                            </div>
                            <div class="form-group">
                                <label for="address">Alamat</label>
                                <textarea class="form-control" name="address" placeholder="Masukkan Alamat Customer" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="address">Keterangan</label>
                                <textarea class="form-control" name="description" placeholder="Masukkan Keterangan"></textarea>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Tambah</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
           $('#kembali').on('click',function (e) {
              e.preventDefault();
              window.location.href="{{url('/admin/customer')}}";
           });
        });
    </script>
@endsection