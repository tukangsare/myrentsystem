@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-2 col-xs-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Payment
                    </div>
                    <div class="panel-body">
                        @if(session('warning'))
                            <div class="alert alert-danger">
                                {{session('warning')}}
                            </div>
                        @endif
                        <form action="{{url("/admin/payment/$order->id/bayar")}}" method="POST" role="form" id="formTambah">
                            <input type="hidden" name="_method" value="PATCH">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="code">Kode Order</label>
                                        <input type="text" class="form-control" name="code" value="{{$order->code}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label for="tanggal">Tanggal Pembayaran</label>
                                        <input type="date" class="form-control" name="tanggal" placeholder="Masukkan Tanggal Pembayaran">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-success">Bayar</button>
                                <button id="kembali" class="btn btn-default">Kembali</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(document).ready(function () {
            $('#kembali').on('click',function (e) {
                e.preventDefault();
                window.location.href="{{url('/admin/payment')}}";
            });
    </script>
@endsection