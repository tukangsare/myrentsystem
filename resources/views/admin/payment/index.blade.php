@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-offset-1 col-xs-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Payment
                    </div>
                    <div class="panel-body">
                        @if(session('status'))
                            <div id="pesan_terselubung" class="alert alert-{{session('status')}}">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                {!!session('msg')!!}
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-xs-2">
                            
                            </div>
                            <div class="col-xs-offset-6 col-xs-4">
                                <form action="{{url('/admin/payment')}}" method="GET">
                                    <div class="input-group">
                                        <input type="text" name="cari" class="form-control" placeholder="Mau mencari sesuatu?">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit">Cari</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12">
                                <table class="table table-responsive">
                                    <thead>
                                    <th>Kode</th>
                                    <th>Customer</th>
                                    <th>Status Bayar</th>
                                    <th>Total Biaya</th>
                                    <th class="text-center">Aksi</th>
                                    </thead>
                                    <tbody>
                                    @if(!$orders)
                                        <tr>
                                            <td class="text-center" colspan="2">Data Tidak Ditemukan</td>
                                        </tr>
                                    @else
                                        @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->code}}</td>
                                                <td>{{$order->customers->name}}</td>
                                                <td>{{$order->payment_status}}</td>
                                                <td>{{$order->total_price}}</td>
                                                <td class="text-center col-xs-3">
                                                    @if($order->getOriginal('payment_status') == false)
                                                    <a href="{{url('/admin/payment/'.$order->id.'/bayar')}}" class="btn btn-success">Bayar</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="nav-bar nav">
                            <span class="total_data pull-left">
                                <strong>Total: {{$total_data}} </strong>
                            </span>
                            <div class="paging pull-right">
                                {{$orders->appends($request->only('cari'))->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('footer')
    <script>
            var a = $('#pesan_terselubung');
            if  (a) {
                window.setTimeout(function() {
                    a.fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove();
                    });
                }, 2000);
            }
        });
    </script>
@endsection