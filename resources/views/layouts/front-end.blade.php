<!DOCTYPE html>
<html lang="en">
<head>
    <title>Malia Sari</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{asset('assets-fashe/images/icons/favicon.png')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/fonts/themify/themify-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/fonts/Linearicons-Free-v1.0.0/icon-font.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/fonts/elegant-font/html-css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/css-hamburgers/hamburgers.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/animsition/css/animsition.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/select2/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/daterangepicker/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/slick/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/vendor/lightbox2/css/lightbox.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets-fashe/css/main.css')}}">
</head>
<body class="animsition">

<!-- Header -->
<header class="header3">
    <!-- Header desktop -->
    <div class="container-menu-header-v3">
        <div class="wrap_header3 p-t-74">
            <!-- Logo -->
            <a href="{{url('/')}}" class="logo3">
                <h3>Malia Sari</h3>
                {{--<img src="images/icons/logo.png" alt="IMG-LOGO">--}}
            </a>
            <?php $total_price = 0;?>
            <!-- Header Icon -->
            <div class="header-icons3 p-t-38 p-b-60 p-l-8">
                @if(is_null(Session::get('id')))
                    <a href="{{route('front.login')}}" class="header-wrapicon1 dis-block">
                        <p class="text-center">
                            <img src="{{asset('assets-fashe/images/icons/icon-header-01.png')}}" class="header-icon1" alt="ICON">
                            <br>
                            Login
                        </p>
                    </a>
                @else
                    <a href="{{route('front.customer.edit')}}" class="header-wrapicon1 dis-block">
                        <p class="text-center">
                            <img src="{{asset('assets-fashe/images/icons/icon-header-01.png')}}" class="header-icon1" alt="ICON"> {{Session::get('username')}}
                            <br>
                            Belanjaan
                        </p>
                    </a>
                @endif
                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <p class="text-center js-show-header-dropdown">
                        <img src="{{asset('assets-fashe/images/icons/icon-header-02.png')}}" class="header-icon1" alt="ICON">
                        <span class="header-icons-noti">{{session('shopcart.prod')?count(session('shopcart.prod')):0}}</span>
                        <br>
                        Belanjaan
                    </p>

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <ul class="header-cart-wrapitem">
                            @if(session('shopcart.prod'))
                                @foreach(session('shopcart.prod') AS $prod_sess)
                                    <li class="header-cart-item">
                                        <div class="header-cart-item-img">
                                            <img src="{{Storage::url($prod_sess['image'])}}" alt="IMG">
                                        </div>

                                        <div class="header-cart-item-txt">
                                            <a href="#" class="header-cart-item-name">
                                                <?php printf('<b>%s</b> - %s',$prod_sess['code'],$prod_sess['name'])?>
                                            </a>

                                            <span class="header-cart-item-info">
											{{$prod_sess['qty']}} x {{'Rp. '.number_format($prod_sess['price'],2,',','.')}}
                                                <?php $total_price = $total_price+($prod_sess['qty']*$prod_sess['price']); ?>
										</span>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <li>Tidak ada daftar belanja</li>
                            @endif
                        </ul>

                        <div class="header-cart-total">
                            Total: {{'Rp. '.number_format($total_price,2,',','.')}}
                        </div>

                        <div class="header-cart-buttons">
                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="{{route('front.cart.show')}}" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    View Cart
                                </a>
                            </div>

                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Check Out
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Menu -->
            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li>
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li>
                            <a href="{{route('front.shop')}}">Shop</a>
                        </li>
                        <li>
                            <a href="{{route('front.contact')}}">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="bottombar flex-col-c p-b-65">
            <div class="t-center">
                Jl. Penuh kenangan No 28, Cirebon
            </div>

            <div class="label label-info">
                Made By Icang
                <!--<a href="htpps://instagram.com/tukangsare" class="topbar-social-item fa fa-instagram"></a>-->
            </div>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="index.html" class="logo-mobile">
            <img src="images/icons/logo.png" alt="IMG-LOGO">
        </a>

        <!-- Button show menu -->
        <div class="btn-show-menu">
            <!-- Header Icon mobile -->
            <div class="header-icons-mobile">
                <a href="#" class="header-wrapicon1 dis-block">
                    <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide2"></span>

                <div class="header-wrapicon2">
                    <img src="images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown" alt="ICON">
                    <span class="header-icons-noti">0</span>

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <ul class="header-cart-wrapitem">
                            <li class="header-cart-item">
                                <div class="header-cart-item-img">
                                    <img src="images/item-cart-01.jpg" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt">
                                    <a href="#" class="header-cart-item-name">
                                        White Shirt With Pleat Detail Back
                                    </a>

                                    <span class="header-cart-item-info">
											1 x $19.00
										</span>
                                </div>
                            </li>

                            <li class="header-cart-item">
                                <div class="header-cart-item-img">
                                    <img src="images/item-cart-02.jpg" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt">
                                    <a href="#" class="header-cart-item-name">
                                        Converse All Star Hi Black Canvas
                                    </a>

                                    <span class="header-cart-item-info">
											1 x $39.00
										</span>
                                </div>
                            </li>

                            <li class="header-cart-item">
                                <div class="header-cart-item-img">
                                    <img src="images/item-cart-03.jpg" alt="IMG">
                                </div>

                                <div class="header-cart-item-txt">
                                    <a href="#" class="header-cart-item-name">
                                        Nixon Porter Leather Watch In Tan
                                    </a>

                                    <span class="header-cart-item-info">
											1 x $17.00
										</span>
                                </div>
                            </li>
                        </ul>

                        <div class="header-cart-total">
                            Total: $75.00
                        </div>

                        <div class="header-cart-buttons">
                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="cart.html" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    View Cart
                                </a>
                            </div>

                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Check Out
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </div>
        </div>
    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu" >
        <nav class="side-menu">
            <ul class="main-menu">
                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							Free shipping for standard order over $100
						</span>
                </li>

                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                    <div class="topbar-child2-mobile">
							<span class="topbar-email">
								fashe@example.com
							</span>

                        <div class="topbar-language rs1-select2">
                            <select class="selection-1" name="time">
                                <option>USD</option>
                                <option>EUR</option>
                            </select>
                        </div>
                    </div>
                </li>

                <li class="item-topbar-mobile p-l-10">
                    <div class="topbar-social-mobile">
                        <a href="#" class="topbar-social-item fa fa-facebook"></a>
                        <a href="#" class="topbar-social-item fa fa-instagram"></a>
                        <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                        <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                        <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                    </div>
                </li>

                <li class="item-menu-mobile">
                    <a href="index.html">Home</a>
                    <ul class="sub-menu">
                        <li><a href="index.html">Homepage V1</a></li>
                        <li><a href="home-02.html">Homepage V2</a></li>
                        <li><a href="home-03.html">Homepage V3</a></li>
                    </ul>
                    <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                </li>

                <li class="item-menu-mobile">
                    <a href="product.html">Shop</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="product.html">Sale</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="cart.html">Features</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="blog.html">Blog</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="about.html">About</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
</header>

<div class="container1-page">
    <!-- Slide1 -->
@yield('content')
    {{--<!-- Top noti -->--}}
    {{--<div class="pos-relative">--}}
        {{--<div class="flex-c-m size22 bg0 s-text21 ab-b-l op-0-9">--}}
            {{--© {{date('Y')}} All rights reserved. | Template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>--}}
            {{--<a href="product.html" class="s-text22 hov6 p-l-5">--}}
                {{--Shop Now--}}
            {{--</a>--}}

            {{--<button class="flex-c-m pos2 size23 colorwhite eff3 trans-0-4 btn-romove-top-noti">--}}
                {{--<i class="fa fa-remove fs-13" aria-hidden="true"></i>--}}
            {{--</button>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>


<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
</div>

<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>


<script type="text/javascript" src="{{asset('assets-fashe/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets-fashe/vendor/animsition/js/animsition.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets-fashe/vendor/bootstrap/js/popper.js')}}"></script>
<script type="text/javascript" src="{{asset('assets-fashe/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="{{asset('assets-fashe/vendor/select2/select2.min.js')}}"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<script type="text/javascript" src="{{asset('assets-fashe/vendor/slick/slick.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets-fashe/js/slick-custom.js')}}"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="{{asset('assets-fashe/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="{{asset('assets-fashe/vendor/lightbox2/js/lightbox.min.js')}}"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="{{asset('asstes-fashe/vendor/sweetalert/sweetalert.min.js')}}"></script>
<script type="text/javascript">
    $('.block2-btn-addcart').each(function(){
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function(){
            swal(nameProduct, "is added to cart !", "success");
        });
    });

    $('.block2-btn-addwishlist').each(function(){
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function(){
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
</script>

<!--===============================================================================================-->
<script src="{{asset('assets-fashe/js/main.js')}}"></script>

</body>
</html>
