@extends('layouts.front-end')
@section('content')
    <div class="container">
        <br>
    <div class="row">
        <div class="col-sm-12 text-right">
            <h2>Login</h2>
        </div>
    </div>
        <div class="clearfix"></div>
        <br>
        <br>
        <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('front.login.post')}}" method="POST">
                    {{csrf_field()}}
                    <div class="form-group">
                            <label for="">Username</label>
                            <input type="text" name="username" class="form-control" style="outline: 1px solid black;">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" name="password" class="form-control" style="outline: 1px solid black;">
                        </div>
                        <div class="text-right">
                            <p><button type="submit" class="btn btn-outline-success">Login</button></p>
                            <br>
                            <p>Belum punya akun? <a href="">Daftar</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection