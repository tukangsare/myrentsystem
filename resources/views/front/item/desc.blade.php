@extends('layouts.front-end')
@section('content')
    <div class="container">
        <br>
    <div class="row">
        <div class="col-sm-12 text-right">
            <h2>Product Desc</h2>
        </div>
    </div>
        <div class="clearfix"></div>
        <br>
        <br>
        <br>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <img class="mx-auto d-block img-fluid" style="max-height:300px;" src="{{\Storage::url($product->images)}}" alt="">
                    <br>
                    <h3>
                    <b>{{$product->code}}</b> - {{$product->name}}                    
                    </h3>                    
                    <br>
                    <p class="text-right" style="color:red;">{{$product->price}}</p>
                    <br>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga corporis soluta impedit eos molestias id iste aperiam aut ea? Doloremque nobis labore nesciunt molestiae nemo. Porro debitis possimus dicta consequatur!</p>
                    <div class="text-right">
                        <a href="{{route('front.shop')}}" class="btn btn-sm btn-default btn-outline-info">Back</a>
                        <a href="{{route('front.cart.add.prod',['prod' => $product->id])}}" class="btn btn-sm btn-default btn-outline-success">Add To Cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection