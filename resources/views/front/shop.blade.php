@extends('layouts.front-end')
@section('content')
    <div class="container">
        <br>
    <div class="row">
        <div class="col-sm-12 text-right">
            @foreach($cats AS $c)
                <a href="{{route('front.shop.cat',['cat' => $c->id])}}" class="btn btn-default btn-outline-danger">{{$c->name}}</a>
            @endforeach
        </div>
    </div>
        <div class="clearfix"></div>
        <br>
        <br>
        <br>
    <div class="row">
        @foreach($product AS $prod)
            <div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
                <!-- Block2 -->
                <div class="block2">
                    <div class="block2-img wrap-pic-w of-hidden pos-relative">
                        <img src="{{Storage::url($prod->images)}}" alt="IMG-PRODUCT">

                        <div class="block2-overlay trans-0-4">
                            <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                            </a>

                            <div class="block2-btn-addcart w-size1 trans-0-4">
                                <a class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4" href="{{route('front.cart.add.prod',['prod' => $prod->id])}}">
                                    Add to Cart
                                </a>
                                <!--
                                <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
                                    Add to Cart
                                </button>
                                -->
                            </div>
                        </div>
                    </div>

                    <div class="block2-txt p-t-20">
                        <a href="{{route('front.prod.desc',['id' => $prod->id])}}" class="block2-name dis-block s-text3 p-b-5">
                            <b>{{$prod->code}}</b> - {{$prod->name}}
                        </a>

                        <span class="block2-price p-r-5">
                            {{$prod->price}} / Hari
                        </span>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    </div>
@endsection