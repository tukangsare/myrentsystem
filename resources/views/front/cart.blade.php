@extends('layouts.front-end')
@section('content')
    <div class="container">
        <br>
    <div class="row">
        <div class="col-sm-12 text-right">
            <!-- <a href="">Check Out</a> -->
        </div>
    </div>
        <div class="clearfix"></div>
        <br>
        <br>
        <br>
    <div class="row">
    <div class="col-sm-12">
    <h4>Cart</h4>
    <br>
    <div class="table-responsive">
            <table class="table table-hovered">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Image</th>
                        <th>Qty</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($carts AS $cart)
                    <tr>
                    <td><b>{{$cart['code']}}</b></td>
                    <td>{{$cart['name']}}</td>
                    <td>{{$cart['price']}}</td>
                    <td width='25%'><img class="img-fluid" src="{{\Storage::url($cart['image'])}}" alt=""></td>
                    <td>{{$cart['qty']}}</td>
                    <td>Aksi</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    </div>
    <div class="text-right">
        <a href="{{route('front.shop')}}" class="btn btn-sm btn-default btn-outline-info">Continue Shopping</a>
        <a href="{{route('front.check_out')}}" class="btn btn-sm btn-default btn-outline-success">Check Out</a>
    </div>
    </div>
    </div>
    </div>
@endsection