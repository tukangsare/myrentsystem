@extends('layouts.front-end')
@section('content')
<section class="slide1 rs1-slick1">
    <div class="wrap-slick1">
        <div class="slick1">
            <div class="item-slick1 item1-slick1" style="background: #f0f0f0;">
                <div class="wrap-content-slide1 size24 flex-col-c-m p-l-15 p-r-15 p-t-120 p-b-170">
                    <h2 class="caption1-slide1 xl-text3 t-center bo15 p-b-3 animated visible-false m-b-25" data-appear="fadeInUp">
                        MALIA SARI
                    </h2>

                    <span class="caption2-slide1 m-text27 t-center animated visible-false m-b-30" data-appear="fadeInDown">
							Penyewaan Kendaraan
						</span>

                    <div class="wrap-btn-slide1 w-size1 animated visible-false" data-appear="zoomIn">
                        <!-- Button -->
                        <a href="{{route('front.shop')}}" class="flex-c-m size2 bo-rad-23 s-text2 bgwhite hov1 trans-0-4">
                            Sewa Sekarang
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection