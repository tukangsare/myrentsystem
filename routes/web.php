<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('front.home');
});

Route::get('/shop','Front\ShopController@index')->name('front.shop');
Route::get('/shop/{cat}','Front\ShopController@indexByCategory')->name('front.shop.cat');
Route::get('/contact','Front\ContactController@index')->name('front.contact');
Route::get('/customer/login','Front\LoginController@index')->name('front.login');
Route::get('/cart','Front\CartController@index')->name('front.cart.show');
Route::get('/checkout','Front\CheckOutController@index')->name('front.check_out');
Route::get('/cart/add/prod/{prod}','Front\CartController@addProduct')->name('front.cart.add.prod');
Route::get('/item/prod/{id}','Front\ProductDetailController@index')->name('front.prod.desc');

Route::post('/customer/login','Front\LoginController@validateLogin')->name('front.login.post');
Route::group(['prefix'=>'as','middleware'=>['auth:customer']],function () {
    Route::get('/ngetes',function() {
        return '123342';
    });
});
Route::get('/asu', function () {
    return password_hash('admin',PASSWORD_DEFAULT);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/admin/home', 'AdminController@index')->name('admin.home')->middleware('roles');
Route::group(['prefix'=>'admin','middleware'=>['auth','roles']],function () {

    Route::get('/', function () {return redirect('admin/home');})->name('admin');
    Route::get('/home', 'AdminController@index')->name('admin.home');
    // Fetch data
    Route::get('/customer','Admin\CustomerController@index')->name('admin.customer');
    Route::get('/return','Admin\ReturnController@index')->name('admin.return');
    Route::get('/order','Admin\OrderController@index')->name('admin.order');
    Route::get('/payment','Admin\PaymentController@index')->name('admin.payment');
    Route::get('/product-type','Admin\ProductTypeController@index')->name('admin.producttype');
    Route::get('/product','Admin\ProductController@index')->name('admin.product');
    Route::get('/p/{id}/detail','Admin\ProductDetailController@index')->name('admin.productdetail');
    Route::get('/o/{id}/detail','Admin\OrderDetailController@index')->name('admin.orderdetail');
    Route::get('/setting/doc-order','Admin\DocumentOrderSettingController@index')->name('admin.setting.doc-order');
    Route::get('/o/{order}/doc','Admin\DocumentOrderController@index')->name('admin.order.doc');
    Route::get('/user','Admin\UserController@index')->name('admin.user');
    Route::get('/employee','Admin\EmployeeController@index')->name('admin.employee');
    // Show form for adding data
    Route::get('/product-type/create','Admin\ProductTypeController@create')->name('admin.producttype.create');
    Route::get('/product/create','Admin\ProductController@create')->name('admin.product.create');
	Route::get('/customer/create','Admin\CustomerController@create')->name('admin.customer.create');
	Route::get('/order/create','Admin\OrderController@create')->name('admin.order.create');
    Route::get('/p/{prod_id}/detail/create','Admin\ProductDetailController@create')->name('admin.productdetail.create');
    Route::get('/o/{prod_id}/detail/create','Admin\OrderDetailController@create')->name('admin.orderdetail.create');
    Route::get('/setting/doc-order/create','Admin\DocumentOrderSettingController@create')->name('admin.setting.doc-order.create');
    Route::get('/o/{order}/doc/create','Admin\DocumentOrderController@create')->name('admin.order.doc.create');
    Route::get('/o/{order}/doc/{doc_setting}/create','Admin\DocumentOrderController@createOne')->name('admin.order.doc.create.one');
    Route::get('/user/create','Admin\UserController@create')->name('admin.user.create');
    Route::get('/employee/create','Admin\EmployeeController@create')->name('admin.employee.create');
    // Save data
    Route::post('/customer/create','Admin\CustomerController@store')->name('admin.customer.create.post');
    Route::post('/product-type/create','Admin\ProductTypeController@store')->name('admin.producttype.create.post');
    Route::post('/product/create','Admin\ProductController@store')->name('admin.product.create.post');
    Route::post('/order/create','Admin\OrderController@store')->name('admin.order.create.post');
    Route::post('/p/{prod_id}/detail/create','Admin\ProductDetailController@store')->name('admin.productdetail.create.post');
    Route::post('/o/{id}/detail/create','Admin\OrderDetailController@store')->name('admin.orderdetail.create.post');
    Route::post('/setting/doc-order/create','Admin\DocumentOrderSettingController@store')->name('admin.setting.doc-order.post');
    Route::post('/o/{order}/doc/{doc_setting}/create','Admin\DocumentOrderController@storeOne')->name('admin.order.doc.post.one');
    Route::post('/o/{order}/doc/post','Admin\DocumentOrderController@store')->name('admin.order.doc.post');
    Route::post('/user/create','Admin\UserController@store')->name('admin.user.create.post');
    Route::post('/employee/create','Admin\EmployeeController@store')->name('admin.employee.post');

    // Deleting data
    Route::delete('/customer/{id}/delete','Admin\CustomerController@destroy')->name('admin.customer.destroy');
    Route::delete('/order/{id}/delete','Admin\OrderController@destroy')->name('admin.order.destroy');
    Route::delete('/product-type/{id}/delete','Admin\ProductTypeController@destroy')->name('admin.producttype.destroy');
    Route::delete('/product/{id}/delete','Admin\ProductController@destroy')->name('admin.product.destroy');
    Route::delete('/p/{prod_id}/detail/{id}/delete','Admin\ProductDetailController@destroy')->name('admin.productdetail.destroy');
    Route::delete('/o/{order_id}/detail/{id}/delete','Admin\OrderDetailController@destroy')->name('admin.orderdetail.destroy');
    Route::delete('/setting/doc-order/{id}/delete','Admin\DocumentOrderSettingController@destroy')->name('admin.setting.doc-order.destroy');
    Route::delete('/user/{id}/delete','Admin\UserController@destroy')->name('admin.user.destroy');
    Route::delete('/employee/{id}/delete','Admin\EmployeeController@destroy')->name('admin.employee.destroy');
    // Show form for editing data
    Route::get('/customer/{id}/edit','Admin\CustomerController@edit')->name('admin.customer.edit');
    Route::get('/product-type/{id}/edit','Admin\ProductTypeController@edit')->name('admin.producttype.edit');
    Route::get('/product/{id}/edit','Admin\ProductController@edit')->name('admin.product.edit');
    Route::get('/p/{prod_id}/detail/{id}/edit','Admin\ProductDetailController@edit')->name('admin.productdetail.edit');
    Route::get('/order/{id}/edit','Admin\OrderController@edit')->name('admin.order.edit');
    Route::get('/setting/doc-order/{id}/edit','Admin\DocumentOrderSettingController@edit')->name('admin.setting.doc-order.edit');
    Route::get('/user/{id}/edit','Admin\UserController@edit')->name('admin.user.edit');
    Route::get('/employee/{id}/edit','Admin\EmployeeController@edit')->name('admin.employee.edit');
    // update data
    Route::patch('/customer/{id}/edit','Admin\CustomerController@update')->name('admin.customer.update');
    Route::patch('/product-type/{id}/edit','Admin\ProductTypeController@update')->name('admin.producttype.update');
    Route::patch('/product/{id}/edit','Admin\ProductController@update')->name('admin.product.update');
    Route::patch('/order/{id}/edit','Admin\OrderController@update')->name('admin.order.update');
	Route::patch('/p/{prod_id}/detail/{id}/edit','Admin\ProductDetailController@update')->name('admin.productdetail.update');
    Route::patch('/setting/doc-order/{id}/edit','Admin\DocumentOrderSettingController@update')->name('admin.setting.doc-order.update');
    Route::patch('/user/{id}/edit','Admin\UserController@update')->name('admin.user.update');
    Route::patch('/employee/{id}/edit','Admin\EmployeeController@update')->name('admin.employee.update');
    // Special routing hehehehe
    Route::get('/o/{order}/doc/{doc_setting}/acc','Admin\DocumentOrderController@acc')->name('admin.order.doc.acc');
    Route::get('/o/{order}/doc/{doc_setting}/download','Admin\DocumentOrderController@download')->name('admin.order.doc.download');
    Route::get('/payment/{id}/bayar','Admin\PaymentController@edit')->name('admin.payment.edit');
    Route::patch('/payment/{id}/bayar','Admin\PaymentController@update')->name('admin.payment.update');
    Route::get('/return/{id}/edit','Admin\ReturnController@edit')->name('admin.payment.edit');
    Route::patch('/return/{id}/edit','Admin\ReturnController@update')->name('admin.payment.update');
    Route::post('/user/{user}/chng_stat','Admin\UserController@changeStatus')->name('admin.user.change_status');
    Route::get('/employee/{employee}/set_user','Admin\EmployeeController@setUserView')->name('admin.employee.set_user');
    Route::post('/employee/{employee}/set_user','Admin\EmployeeController@setUser')->name('admin.employee.set_user.post');
});
Route::get('/404', 'ErrorController@notFound');
